import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import Navbar from './Navbar'

test('Navbar renders correctly', () => {
  const { getByText } = render(
    <Navbar isAuthenticated={true} onSignOut={() => {}} isHomePage={true} />
  )
  const logoElement = getByText(/ITWox/i)
  expect(logoElement).toBeInTheDocument()
})
