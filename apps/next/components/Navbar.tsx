import React from 'react';
import { Button, Spacer, Text, XStack, YStack } from 'tamagui';
import { useRouter } from "next/router";

interface NavbarProps {
    isAuthenticated: boolean;
    onSignOut: () => void;
    isHomePage: boolean;
}

const Navbar: React.FC<NavbarProps> = ({ isAuthenticated, onSignOut, isHomePage }) => {
    const router = useRouter();

    const onSignIn = () => {
        localStorage.removeItem('user');
        router.push('/sign-in');
    }

    const onDashboard = () => {
        router.push('/dashboard');
    }

    return (
        <div style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100px'
        }}>
            <YStack f={1} ai="center" p="$4" space>
                <XStack space="$4">
                    <XStack space="$4" ai="center">
                        <Text>ITWox</Text>
                    </XStack>
                    <Spacer f={1} style={{ flex: 1 }} />
                    {isAuthenticated && isHomePage ? (
                        // <Button onPress={onSignOut}>Sign Out</Button>
                        <Button onPress={onDashboard}>Dashboard</Button>
                    ) : isAuthenticated && !isHomePage ? (
                        <Button onPress={onSignOut}>Sign Out</Button>
                    ) : (
                        <Button onPress={onSignIn}>Sign In</Button>
                    )}
                </XStack>
            </YStack>
        </div>
    );
};

export default Navbar;
