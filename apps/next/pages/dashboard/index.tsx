import Head from "next/head";
import { useRouter } from "next/navigation";
import React, { useEffect, useRef, useState } from "react";
import { useQueries } from "@tanstack/react-query";
import customFetch from "utils/customFetch";
import { Button, XStack, YStack, Spinner } from "tamagui";
import Navbar from "components/Navbar";
import { useAuth } from "context/AuthContext";

const Dashboard = () => {

    const router = useRouter();
    const { user } = useAuth();
    const [currentPage, setCurrentPage] = useState(1);

    const itemsPerPage = 10;
    const isMounted = useRef(true);

    // get post and comments api from env 
    const postApi = process.env.NEXT_PUBLIC_POST_API;
    const commentApi = process.env.NEXT_PUBLIC_COMMENT_API;

    useEffect(() => {
        // Check if user is authenticated, if not, redirect to sign-in
        // Component mounted, set to true
        isMounted.current = true;

        return () => {
            // Component will unmount, set to false
            isMounted.current = false;
        };
    }, []);

    useEffect(() => {
        // Check if the component is mounted and it's not a page refresh
        if (isMounted.current && window.performance.navigation.type !== 1) {
            if (!user.email || !user.password) {
                router.push('/sign-in');
            }
        }
    }, [
        router,
        user.email,
        user.password,
    ]);

    const result = useQueries({
        queries: [
            {
                queryKey: ["dashboard", currentPage],
                queryFn: customFetch.get(`${postApi}?_page=${currentPage}&_limit=${itemsPerPage}`),
            },
            {
                queryKey: ["dashboard"],
                queryFn: customFetch.get(`${commentApi}`),
            },
        ]
    });

    const [posts, comments] = result;

    const { data: postsData, isLoading: isPostDataLoading, isError: isPostDataError, error: postDataError } = posts;
    const { data: commentsData, isLoading: isCommentsLoading, isError: isCommentsError, error: commentsError } = comments;

    const totalPages = Math.ceil(100 / itemsPerPage);

    const handlePageChange = (page: number) => {
        setCurrentPage(page);
    };

    const handleSignOut = () => {
        localStorage.removeItem('user');
        router.push('/sign-in');
    }

    const postCommentsCount = postsData?.map((postItem: any) => {
        const commentsCount = commentsData?.filter((comment: any) => comment.postId === postItem.id).length;

        return {
            postId: postItem.id,
            title: postItem.title,
            body: postItem.body,
            commentsCount: commentsCount
        };
    });

    return (
        <>
            <Head>
                <title>Dashboard</title>
            </Head>
            <div>
                <Navbar
                    isAuthenticated={true}
                    onSignOut={handleSignOut}
                    isHomePage={false}
                />
            </div>
            {
                isPostDataLoading ? <div><Spinner /></div> :
                    <YStack p={50}>
                        <table border={1} style={{
                            fontFamily: "arial, sans-serif",
                            borderCollapse: "collapse",
                            width: "100%"
                        }}>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Comments</th>
                            </tr>
                            {postCommentsCount?.map((post: any) => (
                                <tr key={post.postId}>
                                    <td>{post.postId}</td>
                                    <td>{post.title}</td>
                                    <td>{post.commentsCount}</td>
                                </tr>
                            ))}
                        </table>
                        <div>
                            <YStack space="$4" mt={20}>
                                <XStack space="$4">
                                    <Button disabled={currentPage === 1} onPress={() => handlePageChange(currentPage - 1)}>
                                        Previous
                                    </Button>
                                    <XStack space="$4" ai="center">
                                        <span>Page {currentPage} of {totalPages}</span>
                                    </XStack>
                                    <Button disabled={currentPage === totalPages} onPress={() => handlePageChange(currentPage + 1)}>
                                        Next
                                    </Button>
                                </XStack>
                            </YStack>
                        </div>
                    </YStack>
            }
        </>
    );
}

export default Dashboard;
