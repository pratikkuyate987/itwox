import React from 'react'
import { render, screen } from '@testing-library/react'
import { BrowserRouter as Router } from 'react-router-dom'
import Dashboard from './Dashboard'

test('redirects to sign-in if user is not signed in', () => {
  // Simulate a not-signed-in state
  const authContext = { user: null }

  render(
    <Router>
      <Dashboard authContext={authContext} />
    </Router>
  )

  // Verify that the user is redirected to the sign-in page
  const signInText = screen.getByText(/Sign In/i)
  expect(signInText).toBeInTheDocument()
})

test('renders Dashboard screen for signed-in user', () => {
  // Simulate a signed-in state
  const authContext = { user: { email: 'test@example.com' } }

  render(
    <Router>
      <Dashboard authContext={authContext} />
    </Router>
  )

  // Verify the presence of the title "Dashboard"
  const dashboardTitle = screen.getByText(/Dashboard/i)
  expect(dashboardTitle).toBeInTheDocument()

  // Add more test cases for fetching posts and comments
})

// Add more test cases for other scenarios as needed
