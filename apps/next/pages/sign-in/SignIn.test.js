import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import { BrowserRouter as Router } from 'react-router-dom'
import SignIn from './SignIn'

test('renders Sign In screen', () => {
  render(
    <Router>
      <SignIn />
    </Router>
  )

  // Verify that the top navigation bar is not present
  const navigationBar = screen.queryByRole('navigation')
  expect(navigationBar).not.toBeInTheDocument()

  // Verify the presence of username, password fields, and submit button
  const usernameInput = screen.getByPlaceholderText(/Email/i)
  const passwordInput = screen.getByPlaceholderText(/Password/i)
  const submitButton = screen.getByText(/Submit/i)

  expect(usernameInput).toBeInTheDocument()
  expect(passwordInput).toBeInTheDocument()
  expect(submitButton).toBeInTheDocument()
})

test('validates input fields on form submission', () => {
  render(
    <Router>
      <SignIn />
    </Router>
  )

  // Submit the form without entering any details
  fireEvent.click(screen.getByText(/Submit/i))

  // Verify validation messages
  const usernameError = screen.getByText(/Email is required/i)
  const passwordError = screen.getByText(/Password is required/i)

  expect(usernameError).toBeInTheDocument()
  expect(passwordError).toBeInTheDocument()
})

// Add more test cases for successful sign-in, redirection, etc.

// Add more test cases for other scenarios as needed
