import { Input, Button, Text, YStack, Form } from 'tamagui';
import React from 'react';
import { useForm } from "react-hook-form";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useAuth } from 'context/AuthContext';
import { useRouter } from "next/router";
import Head from 'next/head';

interface SignInProps { }

const MinPasswordLength = 8;
const MaxPasswordLength = 32;

const schema = Yup.object().shape({
    email: Yup.string().email("Email must be valid.").required("Email is required"),
    password: Yup.string()
        .min(MinPasswordLength, `Password should contain at least ${MinPasswordLength} characters`)
        .max(MaxPasswordLength, `Password cannot contain more than ${MaxPasswordLength} characters`)
        .matches(
            /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$#^()+!%*?&])[A-Za-z\d@$#^()+!%*?&]{8,32}$/,
            "Password must have One Uppercase, One Lowercase, One Number and one Special Character. \n Special Characters can be on of @ $ # ^ ( ) + ! % * ? &"
        )
        .required("Password is required"),
});

const SignIn: React.FC<SignInProps> = () => {

    const { setUser } = useAuth();
    const router = useRouter();

    const {
        control,
        register,
        handleSubmit,
        formState: { errors},
    } = useForm({
        resolver: yupResolver(schema),
    });

    const handleSignIn = (formData: any) => {

        setUser({
            email: formData.email,
            password: formData.password,
        });

        router.push('/dashboard');
    };

    return (
        <>
            <Head>
                <title>Sign In</title>
            </Head>
            <YStack f={1} ai="center" jc="center" p="$4">
                <Text fontSize={30} fontWeight="bold" mb="$4">
                    ITWox
                </Text>
                <Form onSubmit={handleSubmit(handleSignIn)}>
                    <YStack space="$4" w="300px">
                        <input
                            {...register("email")}
                            placeholder="Email*"
                            type="email"
                        />
                        {errors.email?.message ? (
                            <Text color="red" fontSize={12}>
                                {errors.email?.message}
                            </Text>
                        ) : null}
                        <input
                            {...register("password")}
                            placeholder="Password*"
                            type="password"
                        />
                        {errors.password?.message ? (
                            <Text color="red" fontSize={12}>
                                {errors.password?.message}
                            </Text>
                        ) : null}
                        <Form.Trigger asChild>
                            <Button >Sign In</Button>
                        </Form.Trigger>
                    </YStack>
                </Form>
            </YStack>
        </>
    );
};

export default SignIn;
