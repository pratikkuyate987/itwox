import axios from "axios";

const tkFetch = {
    get(url: any) {
        return () =>
            axios
                .get(url)
                .then((res) => {
                    if (res.status !== 200) {
                        throw new Error("Something went wrong on our side. Please try again later.");
                    }
                    return res.data;
                })
                .catch((err) => {
                    // status code is not between 200 and 300 , then axiois throws an error, we catch it and throw our message that we have returned from the server
                    const resData = err.response?.data;
                    if (resData && resData.message) {
                        throw new Error(resData.message);
                    } else {
                        throw new Error("Something went wrong on our side. Please try again later.");
                    }
                });

    }
}

export default tkFetch;