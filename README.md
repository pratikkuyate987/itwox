# itwox

# Full-Stack Developer Assignment

## Getting started

1. Fork this repository to your GitLab account.
2. Clone your forked repository to your local machine.
3. Install dependencies:
```
npm install
```
4. Navigate to the Next.js app directory:
```
cd apps/next
```
5. Run the development server:
```
npm run dev
```

## CI/CD Pipeline

The project includes a .gitlab-ci.yml file that defines the following CI/CD steps:

1. Build: Runs npm run build to verify that the code builds successfully.
```
npm run build
```
2. Test: Runs npm test to execute unit tests.
```
npm test
```