import {
  YStack,
  Text,
} from '@my/ui'
import React, { useEffect, useRef, useState } from 'react'
import Navbar from '../../../../apps/next/components/Navbar'
import { useAuth } from '../../../../apps/next/context/AuthContext'

export function HomeScreen() {

  const { user } = useAuth();
  const isMounted = useRef(true);
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  const isHomePage = true;

  useEffect(() => {
    // Check if user is authenticated, if not, redirect to sign-in
    // Component mounted, set to true
    isMounted.current = true;

    return () => {
      // Component will unmount, set to false
      isMounted.current = false;
    };
  }, []);

  useEffect(() => {
    // Check if the component is mounted and it's not a page refresh
    if (isMounted.current) {
      if (user.email || user.password) {
        setIsAuthenticated(true);
      }
    }
  }, [
    user.email,
    user.password,
  ]);

  console.log('isAuthenticated', isAuthenticated);

  return (
    <>
      <Navbar
        isAuthenticated={isAuthenticated}
        onSignOut={() => setIsAuthenticated(false)}
        isHomePage={isHomePage}
      />
      <YStack f={1} jc="center" ai="center" p="$4" space>
        <YStack space="$4" maw={600}>
          <Text>
            WELCOME
          </Text>
        </YStack>
      </YStack>
    </>
  )
}