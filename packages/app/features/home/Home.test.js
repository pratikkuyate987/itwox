import React from 'react'
import { render, fireEvent, screen } from '@testing-library/react'
import { BrowserRouter as Router } from 'react-router-dom'
import HomeScreen from './HomeScreen'

test('renders Home screen for user not signed in', () => {
  render(
    <Router>
      <HomeScreen />
    </Router>
  )

  // Verify the presence of the "Sign In" link
  const signInLink = screen.getByText(/Sign In/i)
  expect(signInLink).toBeInTheDocument()
})

test('renders Home screen for user signed in', () => {
  // Simulate a signed-in state
  const authContext = { user: { email: 'test@example.com' } }

  render(
    <Router>
      <HomeScreen authContext={authContext} />
    </Router>
  )

  // Verify the presence of the "Dashboard" link and "Sign Out" button
  const dashboardLink = screen.getByText(/Dashboard/i)
  const signOutButton = screen.getByText(/Sign Out/i)

  expect(dashboardLink).toBeInTheDocument()
  expect(signOutButton).toBeInTheDocument()
})

// Add more test cases for other scenarios as needed
